import React, { Component } from 'react';
import axios from 'axios';
import './App.css';

class Albums extends Component{
 
  render(){
    return(
      <div>
        <ul>
          {this.props.albums.map(function(album){
            return <li key={album.collectionId}>
							<img src={album.artworkUrl60} alt={album.collectionName} />
								<a href={album.collectionViewUrl} target="_blank">
								{album.collectionName}
								</a>
							</li>;
          })}
        </ul>
      </div>
    )
  }
}

class App extends Component{
  constructor(props){
    super(props)
    this.state={
      userInput: '',
      albums: []
    }
  };
   
 getAlbums = () =>{    
	 axios.get("https://itunes.apple.com/search?term=$" + this.state.userInput + "&entity=album")               
    .then((response) =>{
			this.setState({albums: response.data.results})
			console.log(response);
    })
  };

	handleChange = (e) =>{
		this.setState({userInput: e.target.value});
	};

	handleKeyPress = (e) =>{
		if (e.key === 'Enter') {
			this.getAlbums();
		}
	};

  render(){
    return(
      <div>
        <h1>iTunes Album Search</h1>
				<div className="searchContainer">
        	<input className="search" onChange={this.handleChange} placeholder="Enter artist's name" onKeyPress={this.handleKeyPress}/>
					<button onClick={this.getAlbums}>Search</button>
				</div>
        <Albums albums={this.state.albums} />
      </div>
    )
  }  
}

export default App;